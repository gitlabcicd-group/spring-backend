# Spring-Backend

This project has been created as an step by step to setup a completed pipeline to deploy a Spring REST application in Amazon EKS through Gitlab CICD.

## 1. Spring REST Application

We have a Spring REST application with the follow resources, this spring project have been taken from [here](https://www.springboottutorial.com/spring-boot-react-full-stack-crud-maven-application)

        Get
        /instructors/{username}/courses

        Get
        /instructors/{username}/courses/{id}
        
        Delete
        /instructors/{username}/courses/{id}
	
        Put
        /instructors/{username}/courses/{id}
        
        Post
        /instructors/{username}/courses

## 2. Docker

We use an build the app using [amazon corretto](https://aws.amazon.com/corretto/), the port by default in the tomcat server is 8080.

### Running locally

To run the project locally execute
               
build the docker image

        docker build --tag spring-backend .

run the docker image

        docker run -p 8080:8080 -d spring-backend

Go to a browser and request for 
        http://localhost:8080/instructors/jlrm/courses

## 3. Create EKS cluster

To create the cluster we're going to use [eksctl tool](https://eksctl.io/)

Execute

        eksctl create cluster --name=eks-training --node-type=t2.micro

Once we've finished all these with the following command we're going to delete the cluster

        eksctl delete cluster --name=eks-training


## 4. Connect AWS EKS cluster with Gitlab  --> [source](https://docs.gitlab.com/ee/user/project/clusters/eks_and_gitlab/#configuring-and-connecting-the-eks-cluster)

-  Retrieve certificate
        List the secrets with kubectl get secrets, and one should named similar to default-token-xxxxx. Copy that token name for use below.

        Get the certificate with:
            kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

- Create admin token
        create service of type service account
        create service of type ClusterRoleBinding


## 5. Gitlab CICD file

The file in the root of the project .gitlab-ci.yml contains the stage to build, create the docker image and deploy the app into the EKS cluster, inside these file we use the files into the kubernetes folder _services.yaml_ and _deployment.yaml_


## 6. Kubernetes files

These files are into the folder kubernetes

_deployment.yaml_ : Is for deploy the app to the eks cluster.

_services.yaml_ : Is for create the classic load balancer to reach the app from outside the cluster.


## Don't forget 
To setup the necessary secrets in Gitlab, this is a list of what we are using

- CERTIFICATE_AUTHORITY_DATA
- CI_REGISTRY
- CI_REGISTRY_TOKEN
- CI_REGISTRY_USER
- SERVER
- USER_TOKEN